/*
 * Example: PBKDF2-SHA256 in OpenSSL3
 */
#include <stdio.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/kdf.h>
#include <openssl/core_names.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const unsigned char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

int main(void)
{
	char *password, *hash, *alg;
	unsigned long iterations;
	unsigned char salt[32], key[64];
	EVP_KDF_CTX *ctx;
	EVP_KDF *pbkdf2;

	/* Possible static constructor of parameter list
	OSSL_PARAM params[] = {
		OSSL_PARAM_octet_string(OSSL_KDF_PARAM_PASSWORD, password, strlen(password)),
		OSSL_PARAM_octet_string(OSSL_KDF_PARAM_SALT, salt, sizeof(salt)),
		OSSL_PARAM_ulong(OSSL_KDF_PARAM_ITER, &iterations),
		OSSL_PARAM_utf8_string(OSSL_KDF_PARAM_DIGEST, hash, 0),
		OSSL_PARAM_END
	};
	*/

	/* Or built params dynamically later in code*/
	OSSL_PARAM params[5], *p = params;

	printf("OpenSSL (%s):\n", OpenSSL_version(OPENSSL_VERSION));

	alg = "pbkdf2";
	password = "my passphrase";
	hash = "sha256";
	iterations = 1000;

	//RAND_bytes(salt, sizeof(salt));
	memset(salt, 0xab, sizeof(salt));

	printf("PBKDF2 (%s-%s) using OpenSSL EVP_KDF provider:\n", alg, hash);

	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD, password, strlen(password));
	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT, salt, sizeof(salt));
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_ITER, &iterations);
	*p++ = OSSL_PARAM_construct_utf8_string(OSSL_KDF_PARAM_DIGEST, hash, 0);
	*p++ = OSSL_PARAM_construct_end();

	pbkdf2 = EVP_KDF_fetch(NULL, alg, NULL);
	if (!pbkdf2)
		return 1;

	ctx = EVP_KDF_CTX_new(pbkdf2);
	if (!ctx)
		return 1;

	if (EVP_KDF_derive(ctx, key, sizeof(key), params) != 1)
		return 2;

	EVP_KDF_CTX_free(ctx);
	EVP_KDF_free(pbkdf2);

	hexprint(key, sizeof(key), " ");

	return 0;
}
