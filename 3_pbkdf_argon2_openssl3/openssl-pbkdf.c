/*
 * Example: Argon2id in OpenSSL3.2
 */
#include <stdio.h>
#include <string.h>
#include <openssl/rand.h>
#include <openssl/kdf.h>
#include <openssl/core_names.h>
#include <openssl/thread.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const unsigned char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

int main(void)
{
	char *password, *alg;
	size_t password_len;
	unsigned char salt[16], key[32];
	unsigned long iterations, memory, parallel, threads;
	EVP_KDF_CTX *ctx;
	EVP_KDF *argon2;

	/* params built dynamically later in code*/
	OSSL_PARAM params[8], *p = params;

	printf("OpenSSL (%s):\n", OpenSSL_version(OPENSSL_VERSION));

	alg = "argon2id";
	password ="\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01"
		  "\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01";
	password_len = 32;
	iterations = 3;
	memory = 32;//1024 * 1024;
	parallel = 4;

	/* Threads says how many thread to use, default is no threading */
	if (OSSL_set_max_threads(NULL, parallel) != 1)
		threads = 1;
	else
		threads = parallel;

	//RAND_bytes(salt, sizeof(salt));
	memset(salt, 2, sizeof(salt));

	/* Possible static constructor of parameter list
	OSSL_PARAM params[] = {
		OSSL_PARAM_octet_string(OSSL_KDF_PARAM_PASSWORD, password, password_len),
		OSSL_PARAM_octet_string(OSSL_KDF_PARAM_SALT, salt, sizeof(salt)),
		OSSL_PARAM_ulong(OSSL_KDF_PARAM_ITER, &iterations),
		OSSL_PARAM_ulong(OSSL_KDF_PARAM_THREADS, &threads),
		OSSL_PARAM_ulong(OSSL_KDF_PARAM_ARGON2_LANES, &parallel),
		OSSL_PARAM_ulong(OSSL_KDF_PARAM_ARGON2_MEMCOST, &memory),
		OSSL_PARAM_END
	};
	*/

	printf("PBKDF (%s), max threads %u, using OpenSSL EVP_KDF provider:\n",
	       alg, (unsigned)OSSL_get_max_threads(NULL));

	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD, password, password_len);
	*p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT, salt, sizeof(salt));
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_ITER, &iterations);
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_THREADS, &threads);
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_ARGON2_LANES, &parallel);
	*p++ = OSSL_PARAM_construct_ulong(OSSL_KDF_PARAM_ARGON2_MEMCOST, &memory);
	*p++ = OSSL_PARAM_construct_end();


	argon2 = EVP_KDF_fetch(NULL, alg, NULL);
	if (!argon2)
		return 1;

	ctx = EVP_KDF_CTX_new(argon2);
	if (!ctx)
		return 1;

	if (EVP_KDF_derive(ctx, key, sizeof(key), params) != 1)
		return 2;

	EVP_KDF_CTX_free(ctx);
	EVP_KDF_free(argon2);

	hexprint(key, sizeof(key), " ");

	return 0;
}
