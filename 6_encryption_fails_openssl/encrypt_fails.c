/*
 * Demonstrations of WRONGLY USED encryption modes.
 */
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <string.h>

/* Never ever hardcode secure keys in code ;-) */
const unsigned char KEY[] = {
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x10,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
};

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char IV[]  = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01
};

static void handleErrors(void)
{
	ERR_print_errors_fp(stderr);
	abort();
}

static int encrypt(const EVP_CIPHER *evp_cipher, const unsigned char *plaintext, int plaintext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *ciphertext)
{
	EVP_CIPHER_CTX *ctx;
	int len;
	int ciphertext_len;

	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if (EVP_EncryptInit_ex(ctx, evp_cipher, NULL, key, iv) != 1)
		handleErrors();

	if (EVP_CIPHER_CTX_set_padding(ctx, 0) != 1)
		handleErrors();

	/* If additional authenticated data (AAD) are used:
	* EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len)
	*/

	if (EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len) != 1)
		handleErrors();

	ciphertext_len = len;

	/* Finalise the encryption. Further ciphertext bytes may be written at this stage. */
	if (EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) != 1)
		handleErrors();

	ciphertext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}

static int decrypt(const EVP_CIPHER *evp_cipher, const unsigned char *ciphertext, int ciphertext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *plaintext)
{
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;

	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if (EVP_DecryptInit_ex(ctx, evp_cipher, NULL, key, iv) != 1)
		handleErrors();

	if (EVP_CIPHER_CTX_set_padding(ctx, 0) != 1)
		handleErrors();

	/* If additional authenticated data (AAD) are used:
	* EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len)
	*/

	if (EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) != 1)
		handleErrors();

	plaintext_len = len;

	if (EVP_DecryptFinal_ex(ctx, plaintext + len, &len) != 1)
		handleErrors();

	plaintext_len += len;

	EVP_CIPHER_CTX_free(ctx);

	return plaintext_len;
}

int main(void)
{
	unsigned char plaintext1[128], plaintext2[128], ciphertext1[128], ciphertext2[128], output[128], mangled_iv[16];
	int output_len, ciphertext_len;
	const EVP_CIPHER *evp_cipher;
	int i;

	/* Patterns in ECB, no dependence on position in data */
	printf("\nExample 1: ECB patterns\n");

	memset(plaintext1, 0, sizeof(plaintext1));
	evp_cipher = EVP_aes_256_ecb();
	ciphertext_len = encrypt(evp_cipher, plaintext1, sizeof(ciphertext1), KEY, IV, ciphertext1);
	printf("ECB encrypted ciphertext1 (size = %d) is:\n", ciphertext_len);
	BIO_dump_fp(stdout, (const char *)ciphertext1, ciphertext_len);

	/* CBC first block manipulation through IV */
	printf("\nExample 2: CBC IV bit flips\n");

	memset(plaintext1, 0, sizeof(plaintext1));
	strcpy((char*)plaintext1, "Budget: 1000 EUR\nLoss: 10.25 EUR");
	memcpy(mangled_iv, IV, sizeof(mangled_iv));
	evp_cipher = EVP_aes_256_cbc();
	ciphertext_len = encrypt(evp_cipher, plaintext1, sizeof(ciphertext1), KEY, mangled_iv, ciphertext1);

	output_len = decrypt(evp_cipher, ciphertext1, ciphertext_len, KEY, mangled_iv, output);
	printf("CBC Decrypted plaintext is:\n");
	BIO_dump_fp(stdout, (const char *)output, output_len);

	/* Mangle IV to flip bits in plaintext, see ASCII table */

	/*  0123456789abcdef 0123456789abcdef
	 * "Budget: 1000 EUR\nLoss: 10.25 EUR"); */
	mangled_iv[0x8] ^= '1' ^ '8';
	mangled_iv[0xc] ^= ' ' ^ '0';

	output_len = decrypt(evp_cipher, ciphertext1, ciphertext_len, KEY, mangled_iv, output);
	printf("CBC Decrypted plaintext (mangled IV) is:\n");
	BIO_dump_fp(stdout, (const char *)output, output_len);

	/* Mangle the first ciphertext block for bit flips in the consecutive one. */
	printf("\nExample 3: CBC bit flips in a consecutive block\n");

	/*  0123456789abcdef 0123456789abcdef
	 * "Budget: 1000 EUR\nLoss: 10.25 EUR"); */
	ciphertext1[0x9]  ^=  '.' ^ '0';

	output_len = decrypt(evp_cipher, ciphertext1, ciphertext_len, KEY, mangled_iv, output);
	printf("CBC Decrypted plaintext (mangled ciphertext block) is:\n");
	BIO_dump_fp(stdout, (const char *)output, output_len);

	/* XTS pattern with the constant IV */
	printf("\nExample 4: XTS constant IV block patterns\n");

	RAND_bytes(plaintext1, sizeof(plaintext1));
	memcpy(plaintext2, plaintext1, sizeof(plaintext2));

	evp_cipher = EVP_aes_128_xts();

	ciphertext_len = encrypt(evp_cipher, plaintext1, 32, KEY, IV, ciphertext1);
	printf("XTS Ciphertext1 (size = %d) is:\n", ciphertext_len);
	BIO_dump_fp(stdout, (const char *)ciphertext1, ciphertext_len);

	ciphertext_len = encrypt(evp_cipher, plaintext2, 32, KEY, IV, ciphertext2);
	printf("XTS Ciphertext2 (size = %d) is:\n", ciphertext_len);
	BIO_dump_fp(stdout, (const char *)ciphertext2, ciphertext_len);


	/* CTR stream reuse from plain/ciphertext pair */
	printf("\nExample 5: CTR stream reuse from a plain/ciphertext pair\n");

	RAND_bytes(plaintext1, sizeof(plaintext1));
	strcpy((char*)plaintext2, "A second plaintext.             ");
	evp_cipher = EVP_aes_256_ctr();

	ciphertext_len = encrypt(evp_cipher, plaintext1, sizeof(ciphertext1), KEY, IV, ciphertext1);

	/*
	 * CTR: get a key stream and encrypt second plaintext with it
	 * Note, we do not use encrypt() as we reuse CTR stream.
	 * Stream (nonces) must never be reused!
	 */
	for (i = 0; i < ciphertext_len; i++)
		/*               | get a key from plaintext     | xor it with plaintext2 */
		ciphertext2[i] = plaintext1[i] ^ ciphertext1[i] ^ plaintext2[i];

	/* Show that decryption run as expected with the second plaintext. */
	output_len = decrypt(evp_cipher, ciphertext2, ciphertext_len, KEY, IV, output);
	printf("Decryption of plaintext2 (size = %d) is:\n", output_len);
	BIO_dump_fp(stdout, (const char *)output, output_len);

	/* We encrypted/decrypted plaintext without knowledge of the original KEY! */
	printf("Original Plaintext2 is:\n");
	BIO_dump_fp(stdout, (const char *)plaintext2, ciphertext_len);

	return 0;
}
