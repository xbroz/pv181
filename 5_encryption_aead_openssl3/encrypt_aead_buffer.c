/*
 * OpenSSL3 - Encrypt text buffer using authenticated encryption (AEAD),
 * example based on OpenSSL3 manual/demo
 */
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/core_names.h>
#include <string.h>

/* Never ever hardcode secure keys in code ;-) */
const unsigned char KEY[] = {
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
};

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char IV[]  = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01
};

static void handleErrors(void)
{
	ERR_print_errors_fp(stderr);
	abort();
}

static int encrypt(const unsigned char *plaintext, size_t plaintext_len,
		   unsigned char *ciphertext, unsigned char *tag, size_t tag_len)
{
	EVP_CIPHER *cipher;
	EVP_CIPHER_CTX *ctx;
	int len;
	int ciphertext_len, cipher_tag_len;
	size_t iv_len;
	OSSL_PARAM params[2];

	if (!(cipher = EVP_CIPHER_fetch(NULL, "AES-128-GCM", NULL)))
		handleErrors();

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Set IV length (default is 96 bits)*/
	iv_len = 12;
	params[0] = OSSL_PARAM_construct_size_t(OSSL_CIPHER_PARAM_AEAD_IVLEN, &iv_len);
	params[1] = OSSL_PARAM_construct_end();

	if (EVP_EncryptInit_ex2(ctx, cipher, KEY, IV, params) != 1)
		handleErrors();

	if (EVP_CIPHER_CTX_set_padding(ctx, 0) != 1)
		handleErrors();

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if (EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len) != 1)
		handleErrors();

	ciphertext_len = len;

	/* Finalise the encryption. Further ciphertext bytes may be written at this stage. */
	if (EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) != 1)
		handleErrors();

	ciphertext_len += len;

	cipher_tag_len = EVP_CIPHER_CTX_get_tag_length(ctx);
	if (cipher_tag_len > tag_len) {
		printf("Auth tag buffer is too small.\n");
		cipher_tag_len = tag_len;
	}

	/* Get Auth Tag */
	params[0] = OSSL_PARAM_construct_octet_string(OSSL_CIPHER_PARAM_AEAD_TAG, tag, cipher_tag_len);
	params[1] = OSSL_PARAM_construct_end();

	if (!EVP_CIPHER_CTX_get_params(ctx, params))
		handleErrors();

	EVP_CIPHER_CTX_free(ctx);
	EVP_CIPHER_free(cipher);

	return ciphertext_len;
}

static int decrypt(const unsigned char *ciphertext, size_t ciphertext_len,
		   unsigned char *plaintext, unsigned char *tag, size_t tag_len)
{
	EVP_CIPHER *cipher;
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len, cipher_tag_len;
	size_t iv_len;
	OSSL_PARAM params[2];

	if (!(cipher = EVP_CIPHER_fetch(NULL, "AES-128-GCM", NULL)))
		handleErrors();

	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Set IV length (default is 96 bits)*/
	iv_len = 12;
	params[0] = OSSL_PARAM_construct_size_t(OSSL_CIPHER_PARAM_AEAD_IVLEN, &iv_len);
	params[1] = OSSL_PARAM_construct_end();

	if (EVP_DecryptInit_ex2(ctx, cipher, KEY, IV, params) != 1)
		handleErrors();

	if (EVP_DecryptUpdate(ctx, (unsigned char*)plaintext, &len, ciphertext, ciphertext_len) != 1)
		handleErrors();

	plaintext_len = len;

	cipher_tag_len = EVP_CIPHER_CTX_get_tag_length(ctx);
	if (cipher_tag_len > tag_len) {
		printf("Auth tag buffer is too small.\n");
		cipher_tag_len = tag_len;
	}

	/* Set Auth Tag */
	params[0] = OSSL_PARAM_construct_octet_string(OSSL_CIPHER_PARAM_AEAD_TAG, tag, cipher_tag_len);
	params[1] = OSSL_PARAM_construct_end();

	if (!EVP_CIPHER_CTX_set_params(ctx, params))
		handleErrors();

	if (EVP_DecryptFinal_ex(ctx, plaintext + len, &len) != 1) {
		printf("Auth Tag verify failed.\n");
		handleErrors();
	}

	plaintext_len += len;

	EVP_CIPHER_CTX_free(ctx);
	EVP_CIPHER_free(cipher);

	return plaintext_len;
}

int main(void)
{
	const char *plaintext;
	unsigned char ciphertext[128], decrypted_plaintext[128], tag[32];
	size_t decryptedtext_len, ciphertext_len;

	plaintext = "The quick brown The quick brown fox jumps over the lazy dog";
	printf("Plaintext (length %zd) is:\n%s\n", strlen(plaintext), plaintext);

	/* Encrypt the plaintext */
	ciphertext_len = encrypt((const unsigned char*)plaintext,strlen (plaintext),
				 ciphertext, tag, sizeof(tag));

	printf("Ciphertext (length %zd bytes) is:\n", ciphertext_len);
	BIO_dump_fp(stdout, ciphertext, ciphertext_len);
	printf("AuthTag is:\n");
	BIO_dump_fp(stdout, tag, sizeof(tag));

	/* Decrypt the ciphertext */
	decryptedtext_len = decrypt(ciphertext, ciphertext_len,
				    decrypted_plaintext, tag, sizeof(tag));

	printf("Decrypted text (length %zd) is:\n", decryptedtext_len);
	BIO_dump_fp(stdout, decrypted_plaintext, decryptedtext_len);

	return 0;
}
