/*
 * Example: Password hashing / PBKDF in libsodium
 */
#include <stdio.h>
#include <string.h>
#include <sodium/version.h>
#include <sodium/randombytes.h>
#include <sodium/crypto_pwhash.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

static void print_out(const char *hash, const void *out, unsigned int out_len)
{
	printf("Derived key using %s\n", hash);
	hexprint((const char*)out, out_len, " ");
}

int main(void)
{
	const char *password;
	unsigned char salt[crypto_pwhash_SALTBYTES], key[64];
	unsigned long long cpu_cost;
	size_t mem_cost;
	int alg;

	printf("Sodium (%s) RNG:\n", sodium_version_string());

	password = "my passphrase";
	alg = crypto_pwhash_ALG_DEFAULT; // better crypto_pwhash_ALG_ARGON2ID13 (since 1.0.13)
	cpu_cost = crypto_pwhash_OPSLIMIT_INTERACTIVE; //crypto_pwhash_OPSLIMIT_MODERATE;
	mem_cost = crypto_pwhash_MEMLIMIT_MODERATE; //crypto_pwhash_MEMLIMIT_INTERACTIVE;


	printf("PBKDF alg id: %i, memory cost %zu MiB, cpu cost %llu\n",
		alg, mem_cost / 1024 / 1024, cpu_cost);

	{
		//randombytes_buf(salt, sizeof(salt));
		memset(salt, 0xab, sizeof(salt));

		if (crypto_pwhash(key, sizeof(key), password, strlen(password),
		    salt, cpu_cost, mem_cost, alg))
			return 2;

		print_out("libsodium", key, sizeof(key));
	}

	return 0;
}
