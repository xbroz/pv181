/*
 * Example: HASH and HMAC in OpenSSL 3.0.0 (and later)
 */
#include <stdio.h>
#include <string.h>

/* OpenSSL includes */
#include <openssl/crypto.h>
#include <openssl/core_names.h>
#include <openssl/evp.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

static void print_out(const char *hash, const char *text,
		      const void *expected_out, const void *out,
		      unsigned int out_len)
{
	printf("Input (hash %s): %s\n", hash, text);
	printf("Expected output: ");
	hexprint((const char*)expected_out, out_len, " ");
	printf("Hashed output:   ");
	hexprint((const char*)out, out_len, " ");
}

static int hash(void)
{
	char hash_name[] = "sha256";
	const char text[] = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
	const char *expected_out =
		"\x24\x8d\x6a\x61\xd2\x06\x38\xb8\xe5\xc0\x26\x93\x0c\x3e\x60\x39"
		"\xa3\x3c\xe4\x59\x64\xff\x21\x67\xf6\xec\xed\xd4\x19\xdb\x06\xc1";
	unsigned char out[32];
	unsigned int out_len;

	EVP_MD *hash_id;
	EVP_MD_CTX *md;

	printf("HASH using EVP interface:\n");

	/*
	 * hash_id = EVP_get_digestbyname(hash_name);
	 * You can see also backward compatible call above,
	 * note new EVP_MD_fetch() explicitly fetch
	 * the hash algorithm from a provider.
	 */

	hash_id = EVP_MD_fetch(NULL, hash_name, NULL);
	if (!hash_id)
		return 1;

	if (EVP_MD_size(hash_id) > (int)sizeof(out))
		return 1;

	md = EVP_MD_CTX_new();
	if (!md)
		return 1;

	if (EVP_DigestInit(md, hash_id) != 1)
		return 1;

	if (EVP_DigestUpdate(md, text, strlen(text)) != 1)
		return 2;

	if (EVP_DigestFinal(md, out, &out_len) != 1)
		return 2;

	EVP_MD_CTX_free(md);
	EVP_MD_free(hash_id);

	print_out(hash_name, text, expected_out, out, out_len);

	return 0;
}

static int hmac(void)
{
	char hash_name[] = "sha256";
	const char text[] = "Hi There";
	const unsigned char key[] =
		"\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b";
	const char *expected_out =
		"\xb0\x34\x4c\x61\xd8\xdb\x38\x53\x5c\xa8\xaf\xce\xaf\x0b\xf1\x2b"
		"\x88\x1d\xc2\x00\xc9\x83\x3d\xa7\x26\xe9\x37\x6c\x2e\x32\xcf\xf7";
	unsigned char out[32];
	size_t out_len;

	EVP_MAC *mac;
	EVP_MAC_CTX *md;

	OSSL_PARAM params[] = {
		OSSL_PARAM_utf8_string(OSSL_MAC_PARAM_DIGEST, hash_name, 0),
		OSSL_PARAM_END
	};

	printf("HMAC using EVP interface:\n");

	mac = EVP_MAC_fetch(NULL, OSSL_MAC_NAME_HMAC, NULL);
	if (!mac)
		return 1;

	md = EVP_MAC_CTX_new(mac);
	if (!md)
		return 1;

	if (EVP_MAC_init(md, key, sizeof(key), params) != 1)
		return 1;

	if (EVP_MAC_update(md, (const unsigned char*)text, strlen(text)) != 1)
		return 2;

	if (EVP_MAC_final(md, out, &out_len, sizeof(out)) != 1)
		return 2;

	print_out(hash_name, text, expected_out, out, out_len);

	EVP_MAC_CTX_free(md);
	EVP_MAC_free(mac);

	return 0;
}

int main(void)
{
	int r;

	printf("OpenSSL (%s):\n", OpenSSL_version(OPENSSL_VERSION));

	r = hash();
	if (r)
		return r;

	r = hmac();
	return r;
}
