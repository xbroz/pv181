/*
 * OpenSSL3 examples for ECC, based on manual page examples and demo in git
 */

#include <openssl/bio.h>
#include <openssl/ec.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/core_names.h>

/* Quietly ignore if some attributes cannot be printed here */
static void print_pkey(EVP_PKEY *pkey)
{
	char name[64];
	unsigned char pubkey[256], privkey[256];
	size_t pubkey_len, privkey_len;
	BIGNUM *bn_priv = NULL;

	if (EVP_PKEY_get_utf8_string_param(pkey, OSSL_PKEY_PARAM_GROUP_NAME,
            name, sizeof(name), NULL) == 1)
		printf("Curve name: %s\n", name);

	if (EVP_PKEY_get_octet_string_param(pkey, OSSL_PKEY_PARAM_PUB_KEY,
            pubkey, sizeof(pubkey), &pubkey_len) == 1) {
		printf("Public key:\n");
		BIO_dump_indent_fp(stdout, pubkey, pubkey_len, 2);
	}

	if (EVP_PKEY_get_bn_param(pkey, OSSL_PKEY_PARAM_PRIV_KEY, &bn_priv) == 1 &&
		(privkey_len = BN_bn2bin(bn_priv, privkey))) {
		printf("Private Key:\n");
		BIO_dump_indent_fp(stdout, privkey, privkey_len, 2);
	}

	BN_clear_free(bn_priv);
}

/* Quietly ignore errors here, as not all curves attributes can be exported */
static void export_pkey(EVP_PKEY *pkey)
{
	BIO *bio = BIO_new_fp(stdout, BIO_NOCLOSE);

	BIO_printf(bio, "Key size: %d [bits]\n", EVP_PKEY_bits(pkey));

	PEM_write_bio_PrivateKey(bio, pkey, NULL, NULL, 0, 0, NULL);
	PEM_write_bio_PUBKEY(bio, pkey);

	BIO_free_all(bio);
}

static EVP_PKEY *ec_keygen(char *name)
{
	EVP_PKEY *key = NULL;
	OSSL_PARAM params[2];
	EVP_PKEY_CTX *genctx;

	genctx = EVP_PKEY_CTX_new_from_name(NULL, "EC", NULL);
	if (!genctx) {
		printf("EVP_PKEY_CTX_new_from_name failed\n");
		goto out;
	}

	if (EVP_PKEY_keygen_init(genctx) != 1) {
		printf("EVP_PKEY_keygen_init failed\n");
		goto out;
	}

	params[0] = OSSL_PARAM_construct_utf8_string(OSSL_PKEY_PARAM_GROUP_NAME, name, 0);
	params[1] = OSSL_PARAM_construct_end();

	if (!EVP_PKEY_CTX_set_params(genctx, params)) {
		printf("EVP_PKEY_CTX_set_params failed\n");
		goto out;
	}

	if (EVP_PKEY_generate(genctx, &key) != 1)
		printf("EVP_PKEY_generate failed\n");
out:
	EVP_PKEY_CTX_free(genctx);
	return key;
}

static EVP_PKEY *ed_keygen(char *name)
{
	EVP_PKEY *key = NULL;
	EVP_PKEY_CTX *genctx;

	genctx = EVP_PKEY_CTX_new_from_name(NULL, name, NULL);
	if (!genctx) {
		printf("EVP_PKEY_CTX_new_from_name failed\n");
		goto out;
	}

	if (EVP_PKEY_keygen_init(genctx) != 1) {
		printf("EVP_PKEY_keygen_init failed\n");
		EVP_PKEY_CTX_free(genctx);
		goto out;
	}

	if (EVP_PKEY_generate(genctx, &key) != 1)
		printf("EVP_PKEY_generate failed\n");
out:
	EVP_PKEY_CTX_free(genctx);
	return key;
}

/*
 * ED25519,ED448 - sign and verify C string as an example.
 * Edward curves supports only one-shot interface.
 */
void ed_sign_verify(EVP_PKEY *pkey, const unsigned char *msg)
{
	size_t sig_len, msg_len;
	unsigned char *sig = NULL;
	EVP_MD_CTX *md_sign = NULL, *md_verify = NULL;

	msg_len = strlen((const char*)msg);

	/* Sign message */
	md_sign = EVP_MD_CTX_new();
	if (!md_sign)
		return;

	if (EVP_DigestSignInit(md_sign, NULL, NULL, NULL, pkey) != 1) {
		printf("EVP_DigestSignInit failed\n");
		goto out;
	}

	/* Calculate size for the signature by passing a NULL buffer */
	if (EVP_DigestSign(md_sign, NULL, &sig_len, msg, msg_len) != 1) {
		printf("EVP_DigestSign failed\n");
		goto out;
	}
	sig = OPENSSL_zalloc(sig_len);
	if (!sig) {
		printf("Signature allocation failed\n");
		goto out;
	}

	if (EVP_DigestSign(md_sign, sig, &sig_len, msg, msg_len) != 1) {
		printf("EVP_DigestSign failed\n");
		goto out;
	}

	printf("Signature size %zu [bytes].\n", sig_len);

	md_verify = EVP_MD_CTX_new();
	if (!md_verify)
		return;

	/* Verify signature */
	if (EVP_DigestVerifyInit(md_verify, NULL, NULL, NULL, pkey) != 1) {
		printf("EVP_DigestVerifyInit failed.\n");
		goto out;
	}

	if (EVP_DigestVerify(md_verify, sig, sig_len, msg, msg_len) != 1) {
		printf("EVP_DigestVerify failed.\n");
		goto out;
	}

	printf("Sign & verify OK.\n");
out:
	OPENSSL_free(sig);
	EVP_MD_CTX_free(md_sign);
	EVP_MD_CTX_free(md_verify);
}

int main(void)
{
	EVP_PKEY *pkey;

	/*
	 * There are simple wrappers, but sometimes quite confusing
	 * EVP_EC_gen("ED25519");
	 * EVP_PKEY_Q_keygen(NULL, NULL, "EC", "P-521");
	 *
	 * Curve parameters:
	 * https://www.openssl.org/docs/man3.0/man7/EVP_PKEY-EC.html
	 *	- EC requires OSSL_PKEY_PARAM_GROUP_NAME (EC curve type)
	 * https://www.openssl.org/docs/man3.0/man7/EVP_PKEY-RSA.html
	 *	- RSA requires OSSL_PKEY_PARAM_RSA_BITS
	 * https://www.openssl.org/docs/man3.0/man7/EVP_PKEY-ED448.html
	 *	- Edwards curves do not take any params (set to empty)
	 */

	/*
	 * NIST EC curve example
	 */
	printf("--- NIST P-256 example ---\n");
	pkey = ec_keygen("P-256");
	print_pkey(pkey);
	export_pkey(pkey);
	EVP_PKEY_free(pkey);

	/*
	 * ED25519 example
	 * https://en.wikipedia.org/wiki/EdDSA#Ed25519
	 */
	printf("\n--- ED25519 example ---\n");
	pkey = ed_keygen("ED25519");
	print_pkey(pkey);
	export_pkey(pkey);

	/* Signature has some specific constraints */
	ed_sign_verify(pkey, (const unsigned char*)"message");

	EVP_PKEY_free(pkey);

	return 0;
}
