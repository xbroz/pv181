/* Encrypt text buffer using EVP, example based on OpenSSL3 manual */
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <string.h>

const char *CIPHER_NAME = "AES-256-CBC";

/* Never ever hardcode secure keys in code ;-) */
const unsigned char KEY[] = {
        0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
        0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
};

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char IV[]  = {
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01
};

static void handleErrors(void)
{
	ERR_print_errors_fp(stderr);
	abort();
}

static int encrypt(const unsigned char *plaintext, int plaintext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *ciphertext)
{
	EVP_CIPHER_CTX *ctx;
	EVP_CIPHER *cipher;
	int len;
	int ciphertext_len;

	cipher = EVP_CIPHER_fetch(NULL, CIPHER_NAME, NULL);
	if (!cipher)
		handleErrors();

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits.
	 * You can use helpers to get these parameters:
	 * 	EVP_CIPHER_key_length(EVP_aes_256_cbc())
	 *	EVP_CIPHER_block_size(EVP_aes_256_cbc())
	 *	EVP_CIPHER_iv_length(EVP_aes_256_ecb())
	 *	...
	 */
	if (!EVP_EncryptInit_ex2(ctx, cipher, key, iv, NULL))
		handleErrors();

	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if (EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len) != 1)
		handleErrors();

	ciphertext_len = len;

	/* Finalise the encryption. Further ciphertext bytes may be written at this stage. */
	if (EVP_EncryptFinal_ex(ctx, ciphertext + len, &len) != 1)
		handleErrors();

	ciphertext_len += len;

	EVP_CIPHER_free(cipher);
	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}

static int decrypt(const unsigned char *ciphertext, int ciphertext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *plaintext)
{
	EVP_CIPHER_CTX *ctx;
	EVP_CIPHER *cipher;
	int len;
	int plaintext_len;

	cipher = EVP_CIPHER_fetch(NULL, CIPHER_NAME, NULL);
	if (!cipher)
		handleErrors();

	if (!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	 if (!EVP_DecryptInit_ex2(ctx, cipher, key, iv, NULL))
		handleErrors();

	if (EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len) != 1)
		handleErrors();

	plaintext_len = len;

	if (EVP_DecryptFinal_ex(ctx, plaintext + len, &len) != 1)
		handleErrors();

	plaintext_len += len;

	EVP_CIPHER_free(cipher);
	EVP_CIPHER_CTX_free(ctx);

	return plaintext_len;
}

int main(void)
{
	const unsigned char *plaintext;
	unsigned char ciphertext[128], decrypted_plaintext[128];
	int decryptedtext_len, ciphertext_len;

	/* We intentionally repeat the first block twice - see what happens if ECB is used! */
	/* Also see what happens in CBC mode when you use different IV for decryption. */
	plaintext = (const unsigned char *)"The quick brown The quick brown fox jumps over the lazy dog";

	/* Encrypt the plaintext */
	ciphertext_len = encrypt(plaintext, strlen ((const char *)plaintext), KEY, IV, ciphertext);

	printf("Ciphertext is:\n");
	BIO_dump_fp(stdout, (const char *)ciphertext, ciphertext_len);

	/* Decrypt the ciphertext */
	decryptedtext_len = decrypt(ciphertext, ciphertext_len, KEY, IV, decrypted_plaintext);

	printf("Decrypted text is:\n");
	decrypted_plaintext[decryptedtext_len] = '\0';
	printf("%s\n", decrypted_plaintext);

	return 0;
}
