/*
 * Example: PBKDF2-SHA256 in OpenSSL
 */
#include <stdio.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

static void print_out(const char *hash, const void *out, unsigned int out_len)
{
	printf("Derived key using %s\n", hash);
	hexprint((const char*)out, out_len, " ");
}

int main(int argc, char *argv[])
{
	const char *password;
	unsigned char salt[32], key[64];
	unsigned long iterations;

#if OPENSSL_VERSION_NUMBER < 0x10100000L
        printf("OpenSSL (%s):\n", SSLeay_version(SSLEAY_VERSION));
#else
        printf("OpenSSL (%s):\n", OpenSSL_version(OPENSSL_VERSION));
#endif

	password = "my passphrase";
	iterations = 1000;

	printf("PBKDF2 using OpenSSL:\n");
	{
		//RAND_bytes(salt, sizeof(salt));
		memset(salt, 0xab, sizeof(salt));

		if (!PKCS5_PBKDF2_HMAC(password, strlen(password), salt, sizeof(salt),
			iterations, EVP_sha256(), sizeof(key), key))
			return 2;

		print_out("PBKDF2-SHA256", key, sizeof(key));
	}

	return 0;
}
