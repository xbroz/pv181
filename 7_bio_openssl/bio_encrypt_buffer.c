/* Encrypt text buffer using BIO interface. */
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <string.h>

/* Never ever hardcode secure keys in code ;-) */
const unsigned char KEY[] = {
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f
};

/* And also IV (Initialization vector) should be generated randomly... */
const unsigned char IV[]  = {
    0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01
};

static int encrypt(const unsigned char *plaintext, int plaintext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *ciphertext)
{
	BIO *out = BIO_new(BIO_s_mem());
	BIO *encipher = BIO_new(BIO_f_cipher());
	int ciphertext_len;
	char *ptr;

	BIO *out_encrypted = BIO_push(encipher, out);
	BIO_set_cipher(out_encrypted, EVP_aes_256_cbc(), key, iv, 1);

	BIO_write(out_encrypted, plaintext, plaintext_len);

	BIO_flush(out_encrypted);

	ciphertext_len = BIO_get_mem_data(out_encrypted, &ptr);
	memcpy(ciphertext, ptr, ciphertext_len);

	BIO_free_all(out_encrypted);

	return ciphertext_len;
}

static int decrypt(const unsigned char *ciphertext, int ciphertext_len,
		   const unsigned char *key, const unsigned char *iv, unsigned char *plaintext)
{
	BIO *out = BIO_new(BIO_s_mem());
	BIO *decipher = BIO_new(BIO_f_cipher());
	int plaintext_len;
	BUF_MEM *ptr;

	BIO *out_decrypted = BIO_push(decipher, out);
	BIO_set_cipher(out_decrypted, EVP_aes_256_cbc(), key, iv, 0);

	BIO_write(out_decrypted, ciphertext, ciphertext_len);

	BIO_flush(out_decrypted);

	plaintext_len = BIO_get_mem_data(out_decrypted, &ptr);
	memcpy(plaintext, ptr, plaintext_len);

	BIO_free_all(out_decrypted);

	return plaintext_len;
}

int main(void)
{
	const unsigned char *plaintext;
	unsigned char ciphertext[128], decrypted_plaintext[128];
	int decryptedtext_len, ciphertext_len;

	/* We intentionally repeat the first block twice - see what happens if ECB is used! */
	/* Also see what happens in CBC mode when you use different IV for decryption. */
	plaintext = (const unsigned char *)"The quick brown The quick brown fox jumps over the lazy dog";

	/* Encrypt the plaintext */
	ciphertext_len = encrypt(plaintext, strlen ((const char *)plaintext), KEY, IV, ciphertext);

	printf("Ciphertext is:\n");
	BIO_dump_fp(stdout, (const char *)ciphertext, ciphertext_len);

	/* Decrypt the ciphertext */
	decryptedtext_len = decrypt(ciphertext, ciphertext_len, KEY, IV, decrypted_plaintext);

	printf("Decrypted text is:\n");
	decrypted_plaintext[decryptedtext_len] = '\0';
	printf("%s\n", decrypted_plaintext);

	return 0;
}
