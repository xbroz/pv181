/*
 * Example: Argon2 in libgcrypt (requires gcrypt 1.10)
 *
 * NOTE: This is a really complicated API design, specifically in thread handling.
 */
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <gcrypt.h>

/* Print buffer in HEX with optional separator */
static void hexprint(const char *d, int n, const char *sep)
{
	int i;

	for (i = 0; i < n; i++)
		printf("%02hhx%s", (const char)d[i], sep);
	printf("\n");
}

static void print_out(const char *hash, const void *out, unsigned int out_len)
{
	printf("Derived key using %s\n", hash);
	hexprint((const char*)out, out_len, " ");
}

struct gcrypt_thread_job
{
	pthread_t thread;
	struct job_thread_param {
		gcry_kdf_job_fn_t job;
		void *p;
	} work;
};

struct gcrypt_threads
{
	pthread_attr_t attr;
	unsigned int num_threads;
	unsigned int max_threads;
	struct gcrypt_thread_job *jobs_ctx;
};

static void *gcrypt_job_thread(void *p)
{
	struct job_thread_param *param = p;
	param->job(param->p);
	pthread_exit(NULL);
}

static int gcrypt_wait_all_jobs(void *ctx)
{
	unsigned int i;
	struct gcrypt_threads *threads = ctx;

	for (i = 0; i < threads->num_threads; i++) {
		pthread_join(threads->jobs_ctx[i].thread, NULL);
		threads->jobs_ctx[i].thread = 0;
	}

	threads->num_threads = 0;
	return 0;
}

static int gcrypt_dispatch_job(void *ctx, gcry_kdf_job_fn_t job, void *p)
{
	struct gcrypt_threads *threads = ctx;

	if (threads->num_threads >= threads->max_threads)
		return -1;

	threads->jobs_ctx[threads->num_threads].work.job = job;
	threads->jobs_ctx[threads->num_threads].work.p = p;

	if (pthread_create(&threads->jobs_ctx[threads->num_threads].thread, &threads->attr,
			   gcrypt_job_thread, &threads->jobs_ctx[threads->num_threads].work))
		return -1;

	threads->num_threads++;
	return 0;
}

static gcry_error_t gcrypt_argon2(int type,
	const char *password, size_t password_length,
	const char *salt, size_t salt_length,
	char *key, size_t key_length,
	unsigned long iterations, unsigned long memory, unsigned long parallel)
{
	gcry_error_t err = GPG_ERR_GENERAL;
	gcry_kdf_hd_t hd;
	unsigned long param[4];
	struct gcrypt_threads threads = {
		.max_threads = parallel,
		.num_threads = 0
	};
	const gcry_kdf_thread_ops_t ops = {
		.jobs_context = &threads,
		.dispatch_job = gcrypt_dispatch_job,
		.wait_all_jobs = gcrypt_wait_all_jobs
	};

	param[0] = key_length;
	param[1] = iterations;
	param[2] = memory;
	param[3] = parallel;

	if ((err = gcry_kdf_open(&hd, GCRY_KDF_ARGON2, type, param, 4,
			password, password_length, salt, salt_length,
			NULL, 0, NULL, 0))) {
		free(threads.jobs_ctx);
		return err;
	}

	if (parallel == 1) {
		/* Do not use threads here */
		if ((err = gcry_kdf_compute(hd, NULL)))
			goto out;
	} else {
		threads.jobs_ctx = calloc(threads.max_threads,
				      sizeof(struct gcrypt_thread_job));
		if (!threads.jobs_ctx)
			goto out;

		if (pthread_attr_init(&threads.attr))
			goto out;

		if (gcry_kdf_compute(hd, &ops))
			goto out;
	}

	if ((err = gcry_kdf_final(hd, key_length, key)))
		goto out;
	err = 0;
out:
	gcry_kdf_close(hd);
	pthread_attr_destroy(&threads.attr);
	free(threads.jobs_ctx);

	return err;
}

int main(void)
{
	const char *password;
	size_t password_len;
	char salt[16], key[32];
	unsigned long iterations, memory, parallel;
	gcry_error_t err = 0;

	printf("gcrypt (%s)\n", gcry_check_version(NULL));

	password ="\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01"
		  "\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01";
	password_len = 32;
	iterations = 3;
	memory = 32;//1024 * 1024;
	parallel = 4;

	printf("Argon2 using gcrypt:\n");
	{
		//gcry_randomize(salt, sizeof(salt), GCRY_STRONG_RANDOM);
		memset(salt, 2, sizeof(salt));

		/* GPG_ERR_NO_ERROR is 0 */
		err = gcrypt_argon2(GCRY_KDF_ARGON2ID, password, password_len, salt, sizeof(salt),
				     key, sizeof(key), iterations, memory, parallel);
		if (err) {
			printf("KDF Failure: %s\n", gcry_strerror(err));
			return EXIT_FAILURE;
		}

		print_out("ARGON2ID", key, sizeof(key));
	}

	return EXIT_SUCCESS;
}
